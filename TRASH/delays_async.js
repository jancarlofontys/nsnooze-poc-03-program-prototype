let delayed = [];

let counterDelays = 0;
let limitDelays = 8;

let indexCity = 0;

let delaysInterval; // Interval
delaysInterval = setInterval(() => { // Start Searching
	let cityCode = cities[indexCity].UICCode;
	if(indexCity < cities.length) { // Search until last City
		searchAllDelays(cityCode);
		indexCity++;
	}
}, 300);

if(indexCity < cities.length) { // Stop Interval when last City
	stopSearching();
}


let checker = false;

function stopSearching() {
	clearInterval(delaysInterval);
	checker = true;
}

function searchAllDelays(cityStation) {
	const params = new URLSearchParams({
		maxJourneys: 25,
		lang: "nl",
		uicCode: cityStation
	})
	const url = new URL("http://localhost:8080/https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/departures?" + params);
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '292d0c36e2b54cf5bfbed154b68af7bc');

	const req = new Request(url,
	{
		method: 'GET',
		headers: h,
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) { return res.json();}
			else { throw new Error('Bad HTTP stuff'); }
		})
		.then((jsonData) => {
			let data = jsonData.payload.departures;
			// getDelays(cityStation, data);
			async function hello() {
				try {
					await getDelays();
					return 'world';
				}
				catch (err) {
					console.log('error', err);
				}
			}
			// if(checker === true) {
			// 	setTimeout(() => {
			// 		console.log(delayed);
			// 	}, 3000);
			// }
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
}

function getDelays() {
	let cityDepartures = delays;

	if(counterDelays < limitDelays) {
		
		cityDepartures.forEach(departure => { // Look in every Departure
			const plannedDateTime = new Date(departure.plannedDateTime); // Planned
			const actualDateTime = new Date(departure.actualDateTime); // Actual
		
			if(plannedDateTime.valueOf() != actualDateTime.valueOf()) {

				let difference = actualDateTime.getTime() - plannedDateTime.getTime(); // Delay
				let delayMinutes = new Date(difference).getMinutes();

				processDeparture(origin, departure, plannedDateTime, delayMinutes); // Process Delayed Departure Data
				counterDelays++;
			}
		});
	} else {
		stopSearching();
	}
}

function processDeparture(origin, departure, plannedTime, delay) {
	let endStationsGroup = []; // Group End Staions
	
	let findOriginName = cities.find(c => c.UICCode == origin);
	let originName = findOriginName.namen.lang;
	
	let depDirection = departure.direction; // Departure Direction
	endStationsGroup.push(depDirection)
	
	let routeStations = departure.routeStations; // Route Stations
	routeStations.forEach(route => {
		let findRouteName = cities.find(c => c.UICCode == route.uicCode);
		let routeName = findRouteName.namen.lang;
		endStationsGroup.push(routeName);
	});

	// let plannedTime = departure.plannedDateTime;
	let timeValue = plannedTime.getTime();
	let plannedHour = plannedTime.getHours();
	let pM = plannedTime.getMinutes();
	let plannedMinutes = (pM < 10) ? `0${pM}` : pM; // Minutes
	let timeString = `${plannedHour}:${plannedMinutes} <strong>+${delay}</strong>`; // Build String
	
	// Push to Delayed
	delayed.push({
		origin: originName,
		directions: endStationsGroup,
		timeValue: timeValue,
		timeString: timeString,
		delay: delay
	});

	delayed.sort(function(a,b) {
		return a.timeValue - b.timeValue;
	});
}