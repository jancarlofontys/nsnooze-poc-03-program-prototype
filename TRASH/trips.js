let trip = [];
let fromStation			= "8400530";
let toStation			= "8400058";
time					= "2020-01-18T12:05:00+01:00";
let plannedFromTime		= "2020-01-18T13:05:00+01:00";
let plannedArrivalTime	= "2020-01-18T14:14:00+01:00";

function loadTrip() {
	const ctxCode = `arnu|fromStation=${fromStation}|toStation=${toStation}|plannedFromTime=${plannedFromTime}|plannedArrivalTime=${plannedArrivalTime}|yearCard=false|excludeHighSpeedTrains=false&lang=nl&travelClass=2`;
	// const code = 'arnu%7CfromStation%3D8400530%7CtoStation%3D8400058%7CplannedFromTime%3D2020-01-18T12%3A05%3A00%2B01%3A00%7CplannedArrivalTime%3D2020-01-18T13%3A14%3A00%2B01%3A00%7CyearCard%3Dfalse%7CexcludeHighSpeedTrains%3Dfalse&lang=nl&travelClass=2';
	const ctxUrlCode = encodeURIComponent(ctxCode);
	
	const tripUrl = `http://localhost:8080/https://gateway.apiportal.ns.nl/reisinformatie-api/api/v3/trips/trip?ctxRecon=${ctxUrlCode}`;	
	
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '517fe993539c447e9f154e66c36fc787');
	
	const req = new Request(tripUrl, {
		headers: h,
		method: 'GET',
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) {
				return res.json();
			} else {
				throw new Error('Bad HTTP stuff');
			}
		})
		.then((jsonData) => {
			trip.push(jsonData);
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
}

function getTrip() {
	if(trip.length < 1) {
		loadTrip();
	} else {
		trip = [];
		loadTrip();
	}
}











function getDepartures(originStation) {
	
	const params = new URLSearchParams({
		maxJourneys: 25,
		lang: "nl",
		uicCode: originStation
	})
	const url = new URL("http://localhost:8080/https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/departures?" + params);
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '292d0c36e2b54cf5bfbed154b68af7bc');

	const req = new Request(url,
	{
		method: 'GET',
		headers: h,
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) { return res.json();}
			else { throw new Error('Bad HTTP stuff'); }
		})
		.then((jsonData) => {
			let data = jsonData.payload.departures;
			processDepartures(data);
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
		
	}

function processDepartures(dataDepartures) {

	let departures = dataDepartures;

	const endStation = cities.find(city => city.UICCode == tripEnd);
	let lngName = endStation.namen.lang;
	let medName = endStation.namen.middel;

	let endStationNames = [];
	endStationNames.push(lngName);
	endStationNames.push(medName);

	let depAllStopsNames = [];
	
	departures.forEach(departure => {

		depAllStopsNames.push(departure.direction)
		let routes = departure.routeStations;
		routes.forEach(route => {
			depAllStopsNames.push(route.mediumName)
		});

		depAllStopsNames.forEach(stopName => {
			compare(stopName);
			// endStationNames.forEach(stationName => {
			// 	if(stationName == stopName) {
			// 		console.log("object");
			// 	}
			// });
		});

		// let departureTrue = depAllStopsNames.find(station => endStationNames.includes(station));

		// if(departureTrue) {
		// 	// console.log(departure.plannedDateTime);
		// }

		
	});

	console.log(endStationNames);
	// console.log("object");
	// console.log(depAllStopsNames);

	function compare(name) {
		// endStationNames.forEach(stationName => {
		// 	if(name == stationName) {
		// 		console.log(name);
		// 	}
		// });
		const found = endStationNames.find(stationName => stationName == name )
		// const found = cities.find(city => city.UICCode == tripEnd);
		if(found) {
			console.log(name);
		}
	}

	// let depAllStopsNames = []; // directions Object

	// // let results = cities.filter(x => x.UICCode.includes(tripEnd));
	// // console.log(results.code);

	// const endStation = cities.find(city => city.UICCode == tripEnd); // endStation Object
	// // let lngName = endStation.namen.lang;
	// // let medName = endStation.namen.middel;

	// departures.forEach(departure => { // get Departure Directions
	// 	let directionName = departure.direction;
	// 	// let routeStationsName = departure.routeStations.mediumName;
	// 	let routeStations = departure.routeStations;

	// 	routeStations.forEach(route => {
	// 		console.log(route.mediumName);
	// 	});

	// 	// console.log(routeStationsName);
	// 	depAllStopsNames.push({
	// 		direction: directionName,
	// 		routes: routeStationsName
	// 	});
	// 	// depAllStopsNames.push(routeStationsName.mediumName);
	// });

	// // console.log(depAllStopsNames);

	// // console.log(departures);

	// depAllStopsNames.forEach(name => {
	// 	if(name == lngName || name == medName) {
	// 		console.log(name);
	// 		// console.log("Jesus Christ");
	// 	}
	// });


	// if(myStation) {
	// 	console.log("Jesus Christ");
	// }

	// depDirections.forEach(element => {
	// 	const d = cities.find(city => city.UICCode == tripEnd);
	// });
	// const d = cities.find(city => city.UICCode == tripEnd);

	// const endStation = depDirections.find(station => station == tripEnd);

	// console.log(endStation);


	departures.forEach(departure => { // In every Departure

		// console.log(tripEnd);
		// console.log(departure.direction);
		// console.log(departure.routeStations);

		// // Get Times
		// const plannedDateTime = new Date(departure.plannedDateTime); // Planned
		// const actualDateTime = new Date(departure.actualDateTime); // Actual

		// // If there is a Delay
		// if(plannedDateTime.valueOf() != actualDateTime.valueOf()) {
		// 	let difference = actualDateTime.getTime() - plannedDateTime.getTime(); // Calculate Delay
		// 	alarmDelay = new Date(difference).getMinutes(); // Convert to Minutes
		// 	console.log(alarmDelay); // Log Delay Quantity in minutes
		// }
	});
}