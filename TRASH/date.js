function getCities() {
	const cities = [];
	const citiesUrl = 'cities.json';

	fetch(citiesUrl)
		.then((res) => {
			if(res.ok) {
				return res.json();
			} else {
				throw new Error('Bad HTTP stuff');
			}
		})
		.then((jsonData) => {
			let data = jsonData.payload;
			cities.push(data);
		})
	return cities;
}

const disruptions = {
	werkzaamheden: [],
	storingen: []
};
function getDisruptions() {
	const disruptionsUrl = 'http://localhost:8080/https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/disruptions';

	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '517fe993539c447e9f154e66c36fc787');

	const req = new Request(disruptionsUrl, {
		headers: h,
		method: 'GET',
	});

	fetch(req)
		.then((res) => {
			if(res.ok) {
				return res.json();
			} else {
				throw new Error('Bad HTTP stuff');
			}
		})
		.then((jsonData) => {
			const data = jsonData.payload;
			disruptions["werkzaamheden"] = data.filter(d => d.type === "werkzaamheid");
			disruptions["storingen"] = data.filter(d => d.type === "verstoring");
		})
		.then(() => {
			makeMatch();
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
}

const matches = [];
function makeMatch() {

	disruptions["werkzaamheden"].forEach(di => {
		const startTime = new Date(di.verstoring.trajecten[0].begintijd);
		const endTime = new Date(di.verstoring.trajecten[0].eindtijd);

		if(startTime <= time && time <= endTime) {
			matches.push(di.titel);
		}
		
	});
	console.log(matches);
}

let time;
function setTime(h) {
	let d = new Date();
	let hour = (h) ? h : d.getHours();
	let date = d.getDate();
	let m = d.getMonth() + 1;
	let month = (m < 10) ? `0${m}` : m;
	let year = d.getFullYear();
	
	let timeString = year + "-" + month + "-" + date + "T" + hour + ":00:00" + "+0100";
	time = new Date(timeString);
}


function createTimer() {
	const selectTime = document.getElementById("timeSelection");
	for (let i = 0; i < 25; i++) {
		const option = document.createElement("option");
		let hour = "";
		(i<10) ? hour = `0${i}` : hour = `${i}`;
		option.text = `${hour}:00`;
		option.value = `${hour}`;
		selectTime.appendChild(option);
	}
	
	setTime(selectTime.value);
	// makeMatch();
	
	selectTime.addEventListener("change", function() {
		setTime(selectTime.value);
		makeMatch();
	}, false);
}

window.onload = () => {
	getDisruptions();
	createTimer();

	// let d1 = new Date("2020-01-20T05:00:00+0100");
	// let d2 = new Date("2020-01-20T04:00:00+0100");
	
	// if (d1>d2) console.log("d1 > d2");
	// else if (d2>d1) console.log("d2 > d1");

	// makeMatch();
}

// arnu
// %7C
// fromStation
// %3D
// 8400045
// %7C
// toStation
// %3D
// 8400282
// %7C
// plannedFromTime
// %3D
// 2020-01-18T17
// %3A
// 30
// %3A
// 00
// %2B
// 01
// %3A
// 00
// %7C
// plannedArrivalTime
// %3D
// 2020-01-18T19
// %3A
// 50%
// 3A00
// %2B
// 01
// %3A
// 00
// %7C
// yearCard
// %3D
// false
// %7C
// excludeHighSpeedTrains
// %3D
// false