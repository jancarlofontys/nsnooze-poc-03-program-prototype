// // let delayed = [];
// let delayedGroup = [];

// // Get Origin Station, Directions[], Planned Time, Delay
// function makeSuggestions(sugStation, sugDeparture, sugTime, sugDelay) {
// 	let sugOrigin;
// 	let sugDirections = [];
	
// 	// Get Origin City Name
// 	cities.forEach(city => {
// 		if(city.UICCode == sugStation) {
// 			sugOrigin = city.namen.lang;
// 		}
// 	});
	
// 	// Group All Directions intro String
// 	sugDirections.push(sugDeparture.direction) // Directions
// 	let routeStations = sugDeparture.routeStations; // Directions + routeStations
// 	routeStations.forEach(routeStation => {
// 		sugDirections.push(routeStation.mediumName)
// 	});
// 	let directionsString = sugDirections.join(", "); // Build String
	
// 	// Get Departure Time Hours and minutes
// 	let plannedHour = sugTime.getHours(); // Hours
// 	let pM = sugTime.getMinutes();
// 	let plannedMinutes = (pM < 10) ? `0${pM}` : pM; // Minutes
// 	let plannedTimeString = `${plannedHour}:${plannedMinutes} <strong>+${sugDelay}</strong>`; // Build String
	
// 	// Group Suggestion
// 	delayedGroup.push({
// 		origin: sugOrigin,
// 		directions: directionsString,
// 		plannedTime: sugTime.getTime(),
// 		plannedTimeString: plannedTimeString
// 	});
// 	// Store Group of suggestions
// 	// delayed.push(delayedGroup);
// 	delayed = delayedGroup;
// 	delayed.sort(function(a,b) {
// 		return a.plannedTime - b.plannedTime;
// 	})
// 	setTimeout(() => {
// 		sugToTable();
// 	}, 10000);
// }

// function sugToTable() {
// 	delayedGroup.forEach(d => {
// 		printToTable(d.origin, d.directions, d.plannedTimeString); // Fill in Table
// 	});
// }

function printToTable(o,d,t) {
	const table = document.getElementById("table"); // The Table
	const tableRow = document.createElement("tr"); // Create Rows
	
	const tdOrigin = document.createElement("td");
	tdOrigin.innerHTML = o;

	const tdDirections = document.createElement("td");
	tdDirections.innerHTML = d.join(", ");
	tdDirections.innerHTML = d;
	
	const tdTime = document.createElement("td");
	tdTime.innerHTML = t;

	table.appendChild(tableRow);		// Append Rows to Table
	tableRow.appendChild(tdOrigin);		// Append Origin to Rows
	tableRow.appendChild(tdDirections); // Append Directions to Rows
	tableRow.appendChild(tdTime);		// Append Time to Rows
}