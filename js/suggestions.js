let delayed = [];

function makeSuggestions(sugStation, sugDeparture, sugTime, sugDelay) {
	let sugOrigin;
	let sugDirections = [];
	let sugPlannedDate;

	cities.forEach(city => { // Origin
		if(city.UICCode == sugStation) {
			sugOrigin = city.namen.lang;
		}
	});
	
	sugDirections.push(sugDeparture.direction) // Directions
	
	let routeStations = sugDeparture.routeStations; // Directions + routeStations
	routeStations.forEach(routeStation => {
		sugDirections.push(routeStation.mediumName)
	});

	sugPlannedDate = sugTime.toTimeString(); // Planned At
	
	let pM = sugTime.getMinutes();
	let plannedMinutes = (pM < 10) ? `0${pM}` : pM;
	let plannedHour = sugTime.getHours();
	let plannedTime = `${plannedHour}:${plannedMinutes} <strong>+${sugDelay}</strong>`; // Planned At in Hour:Minute Format
	
	delayed.push({ // Create Keys
		origin: sugOrigin,
		directions: sugDirections,
		plannedDeparture: sugPlannedDate
	});
	
	// console.log(delayed);

	printToTable(sugOrigin, sugDirections, plannedTime); // Fill in Table
}

function printToTable(o,d,t) {
	const table = document.getElementById("table"); // The Table
	const tableRow = document.createElement("tr"); // Create Rows
	
	const tdOrigin = document.createElement("td");
	tdOrigin.innerHTML = o;

	const tdDirections = document.createElement("td");
	tdDirections.innerHTML = d.join(", ");
	
	const tdTime = document.createElement("td");
	tdTime.innerHTML = t;

	table.appendChild(tableRow);		// Append Rows to Table
	tableRow.appendChild(tdOrigin);		// Append Origin to Rows
	tableRow.appendChild(tdDirections); // Append Directions to Rows
	tableRow.appendChild(tdTime);		// Append Time to Rows
}