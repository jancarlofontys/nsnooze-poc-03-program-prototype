function getDepartures(originStation) {
	
	const params = new URLSearchParams({
		maxJourneys: 25,
		lang: "nl",
		uicCode: originStation
	})
	const url = new URL("http://localhost:8080/https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/departures?" + params);
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '292d0c36e2b54cf5bfbed154b68af7bc');

	const req = new Request(url,
	{
		method: 'GET',
		headers: h,
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) { return res.json();}
			else { throw new Error('Bad HTTP stuff'); }
		})
		.then((jsonData) => {
			let data = jsonData.payload.departures;
			processDepartures(data);
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
		
	}

let checkMargin = 10;
function processDepartures(dataDepartures) {

	let departures = dataDepartures; // Incorporate Request Data

	departures.forEach(departure => { // Loop in every Departure

		const plannedDateTime = new Date(departure.plannedDateTime); // Planned
		const actualDateTime = new Date(departure.actualDateTime); // Actual
		// Checks Direction
		let direction = departure.direction;
		let directionCity = cities.find(city => city.namen.lang == direction);
		let directionCode = directionCity.UICCode;
		if(directionCode == tripEnd) {
			checkDepartureDelay(plannedDateTime, actualDateTime); // Check
		}
		// Checks Routes
		let routeStations = departure.routeStations;
		routeStations.forEach(route => { // Check Every Route
			let routeName = route.mediumName;
			let routeCity = cities.find(city => city.namen.middel == routeName);
			let routeCode = routeCity.UICCode;
			if(routeCode == tripEnd) {
				checkDepartureDelay(plannedDateTime, actualDateTime); // Check
			}
		});
	});

	function checkDepartureDelay(planned, actual) { // Check if there is alarmDelay
		if(planned.valueOf() != actual.valueOf()) {
			let timePlanned = planned.getMinutes(); // Departure Time
			let timeStart = new Date().getMinutes(); // Now
			let timeMargin = new Date().getMinutes() + checkMargin; // Margin
			// Departure Time < now + margin
			if(timePlanned < timeStart + timeMargin){
				let difference = actual.getTime() - planned.getTime(); // Calculate alarmDelay
				alarmDelay = new Date(difference).getMinutes(); // Convert to Minutes
				console.log(alarmDelay); // Log alarmDelay Quantity in minutes
				// if(alarmDelay) {
					let span = document.createElement("span");
					span.innerHTML = ` <span>+${alarmDelay}</span>`;
					domAlarm.appendChild(span);
				// }
			}
		}
	}
}