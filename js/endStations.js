function getDepartures(originStation) {
	
	const params = new URLSearchParams({
		maxJourneys: 25,
		lang: "nl",
		station: originStation
	})
	const url = new URL("http://localhost:8080/https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/departures?" + params);
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '292d0c36e2b54cf5bfbed154b68af7bc');

	const req = new Request(url,
	{
		method: 'GET',
		headers: h,
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) { return res.json();}
			else { throw new Error('Bad HTTP stuff'); }
		})
		.then((jsonData) => {
			let data = jsonData.payload.departures;
			processDepartures(data);
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
		
	}

function processDepartures(dataDepartures) {

	let departures = dataDepartures;

	let endStationCode = tripEnd;
	let names = []
	cities.forEach(city => {
		if(city.UICCode = endStationCode) {
			names.push(city.namen.lang);
			names.push(city.namen.middel);
		}
	});

	let directions = []
	departures.forEach(departure => {
		const direction = departure.direction;
		const routeStation = departure.routeStation.mediumName
		directions.push(direction);
		directions.push(routeStation);
	});
	
	directions.forEach(d => {
		if(d == names[d.indexOf()]) {
			console.log("true");
		} else {
			console.log("false");
		}
	});

	const hasStation = directions.some(i=> names.includes(i));
	// const hasStation = directions.some(r=> names.indexOf(r) >= 0)
	// const hasStation = directions.some((val) => names.indexOf(val) !== -1);
	
	if(hasStation) {
		console.log("hasStation");
		departures.forEach(departure => { // In every Departure

			// Get Times
			const plannedDateTime = new Date(departure.plannedDateTime); // Planned
			const actualDateTime = new Date(departure.actualDateTime); // Actual

			// If there is a Delay
			if(plannedDateTime.valueOf() != actualDateTime.valueOf()) {
				let difference = actualDateTime.getTime() - plannedDateTime.getTime(); // Calculate Delay
				alarmDelay = new Date(difference).getMinutes(); // Convert to Minutes
				console.log(alarmDelay); // Log Delay Quantity in minutes
			}
		});
	}
}