let tripStart = "";
let tripEnd = "";

function trainSelection() {
	tripStart = cities[0].UICCode;
	tripEnd = cities[0].UICCode;
	
	const startStation = appendStation(document.getElementById("startStation"));
	const endStation = appendStation(document.getElementById("endStation"));
	
	function appendStation(station) {
		cities.forEach(city => {
			const option = document.createElement("option");
			
			const cityName = city.namen.lang;
			const cityCode = city.UICCode;
			// const cityCode = city.code;
			option.text = cityName;
			option.value = cityCode;
	
			station.appendChild(option);
		});
		return station;
	}
	
	startStation.addEventListener("change", function() {
		tripStart = startStation.value;
		// console.log(`Start Station: ${startStation.value}`);
	}, false);
	
	endStation.addEventListener("change", function() {
		tripEnd = endStation.value;
		// console.log(`End Station: ${endStation.value}`);
	}, false);
}