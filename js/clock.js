function createClock() {
	let today = new Date();
	let h = today.getHours();
	let m = today.getMinutes();
	let s = today.getSeconds();
	m = checkClock(m);
	s = checkClock(s);
	document.getElementById("clock").innerHTML = h + ":" + m + ":" + s;
	let t = setTimeout(createClock, 500);
}

function checkClock(i) {
	if (i < 10) {
		i = "0" + i // add zero in front of numbers < 10
	};
	return i;
}