let alarmTime;
function setTime(hrs=0, mts=0) {
	let d = new Date();
	
	let year = d.getFullYear();
	let monthIndex = d.getMonth() + 1;
	let month = (monthIndex < 10) ? `0${monthIndex}` : monthIndex;
	let date = d.getDate();
	let hour = (hrs) ? hrs : d.getHours();
	let minutes = (mts) ? mts : d.getMinutes();
	
	let timeString = year + "-" + month + "-" + date + "T" + hour + ":" + minutes + ":00" + "+01:00";
	// console.log(timeString);
	domAlarm.innerHTML = `${hour}:${minutes}:00`;
	alarmTime = new Date(timeString);
}

const hourSelection = document.getElementById("hours");
const minuteSelection = document.getElementById("minutes");
function createTimeOptions() {
	
	for (let i = 0; i < 24; i++) {
		const option = document.createElement("option");
		let hour = "";
		(i<10) ? hour = `0${i}` : hour = `${i}`;
		option.text = `${hour}`;
		option.value = `${hour}`;
		hourSelection.appendChild(option);
	}

	for (let i = 0; i < 61; i++) {
		const option = document.createElement("option");
		let minute = "";
		(i<10) ? minute = `0${i}` : minute = `${i}`;
		option.text = `${minute}`;
		option.value = `${minute}`;
		minuteSelection.appendChild(option);
	}
}

function changeTime() {
	setTime(hourSelection.value, minuteSelection.value);
}