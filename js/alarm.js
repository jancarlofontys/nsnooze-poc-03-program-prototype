const sound = new Audio();
sound.src = 'alarm.mp3';
let timer;
let alarmDelay;
let domAlarm = document.getElementById("alarm");

function initAlarm(){
	setTimeout(() => {
		domAlarm.classList.add("is-ringing");
		sound.loop = true;
		sound.play();
	}, 60000 * alarmDelay); // If there's no delay fire up inmediatly
}

let duration;

if(alarmDelay) {
	let span = document.createElement("span");
	span.innerHTML = ` <span>+${alarmDelay}</span>`;
	domAlarm.appendChild(span);
}

function setAlarm(){
	
	duration = alarmTime.getTime() - (new Date()).getTime();
	if(duration > 0) {
		timer = setTimeout(initAlarm, duration)
		// DONT DELETE
		let checktime = duration - (60000*2);
		setTimeout(() => {
			getDepartures(tripStart);
		}, checktime);
	}
	
	if(isNaN(alarmTime)){
		alert("Invalid DateTime");
		return;
	}

	if(duration < 0){
		alert('Time is already passed');
		return;
	}
}

function stopAlarm(){
	sound.pause();
	sound.currentTime = 0;
}

function snoozeAlarm(){
	stopAlarm();
	setTimeout(initAlarm, 60000 * alarmDelay);
}