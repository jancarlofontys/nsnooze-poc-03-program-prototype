const cities = [];
function getCities() {
	const citiesUrl = '/js/cities.json';

	fetch(citiesUrl)
		.then((res) => {
			if(res.ok) {
				return res.json();
			} else {
				throw new Error('Bad HTTP stuff');
			}
		})
		.then((jsonData) => {
			let data = jsonData.payload;
			data.forEach(element => {
				if(element.land == "NL") {
					cities.push(element);
				}
			});
			// cities.push(...data);
		})
}