let delaysInterval; // Variable for Interval

let counterDelay = 0; // Count Delayed trains in processDelays
let limitDelays = 5; // Count Delayed trains until limit

let indexCity = 0;
delaysInterval = setInterval(() => { // Start Searching
	let cityCode = cities[indexCity].UICCode;
	if(indexCity < cities.length) { // Search until last City
		searchAllDelays(cityCode);
		indexCity++;
	}
}, 300);

if(indexCity < cities.length) { // Stop Interval when last City
	stopSearching();
}

function stopSearching() {
	clearInterval(delaysInterval);
}

function searchAllDelays(cityStation) {
	const params = new URLSearchParams({
		maxJourneys: 25,
		lang: "nl",
		uicCode: cityStation
	})
	const url = new URL("http://localhost:8080/https://gateway.apiportal.ns.nl/public-reisinformatie/api/v2/departures?" + params);
	const h = new Headers();
	h.append('Ocp-Apim-Subscription-Key', '292d0c36e2b54cf5bfbed154b68af7bc');

	const req = new Request(url,
	{
		method: 'GET',
		headers: h,
	});
	
	fetch(req)
		.then((res) => {
			if(res.ok) { return res.json();}
			else { throw new Error('Bad HTTP stuff'); }
		})
		.then((jsonData) => {
			let data = jsonData.payload.departures;
			processDelays(data, cityStation);
		})
		.catch((err) => {
			console.log('ERROR:', err.message);
		});
}

function processDelays(delays, sugStat) {
	let cityDepartures = delays;

	if(counterDelay < limitDelays) {
		
		cityDepartures.forEach(departure => { // Look in every Departure
			const plannedDateTime = new Date(departure.plannedDateTime); // Planned
			const actualDateTime = new Date(departure.actualDateTime); // Actual
		
			if(plannedDateTime.valueOf() != actualDateTime.valueOf()) { // If there is a Delay
				let difference = actualDateTime.getTime() - plannedDateTime.getTime(); // Calculate Delay
				let delayForSuggestion = new Date(difference).getMinutes(); // Convert to Minutes
				makeSuggestions(sugStat, departure, plannedDateTime, delayForSuggestion); // Add delayed Departures to Suggestions
				counterDelay++;
			}
		});
	} else {
		stopSearching();
	}
}